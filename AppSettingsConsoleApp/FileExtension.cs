﻿using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AppSettingsConsoleApp
{
    public static class FileExtension
    {
        public static string SetPath()
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            var section = configuration.GetSection("FileSetting").Get<FileSetting>();

            IServiceCollection services = new ServiceCollection();

            services.AddSingleton(section);

            var file = Directory.GetFiles(section.Address).FirstOrDefault(s => s.EndsWith(".csv"));
            return file;
        }
    }
}
