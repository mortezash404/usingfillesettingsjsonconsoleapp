﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AppSettingsConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = FileExtension.SetPath();

            var persons = new List<Person>();

            using (var streamReader = new StreamReader(file))
            {
                while (!streamReader.EndOfStream)
                {
                    var lines = streamReader.ReadLine()?.Split(',');

                    var person = new Person
                    {
                        Name = lines[0],
                        Phone = lines[1],
                        City = lines[2]
                    };

                    persons.Add(person);
                }
            }

            foreach (var person in persons)
            {
                Console.WriteLine($"Name is : {person.Name}, Phone is : {person.Phone}, City is : {person.City}");
            }
        }
    }
}
